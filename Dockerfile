FROM splservices/baseimage

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF

RUN "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | tee /etc/apt/sources.list.d/mono-official-stable.list

RUN apt-get update -y

RUN apt-get install tzdata -y
RUN dpkg-reconfigure -f noninteractive tzdata

RUN apt-get install bash git curl wget apt-utils \
  python-pip libxml2-dev libxslt-dev lib32z1-dev \
  python-lxml apt-utils locales \
  nodejs npm moreutils jq unzip -y

RUN apt-get install libcurl4-openssl-dev -y

RUN apt-get install mono-complete \
  libcurl3 -y

RUN apt-get install pandoc -y

RUN npm install -g tap-xunit
RUN npm install -g bats

RUN wget https://github.com/GitTools/GitVersion/releases/download/v4.0.0-beta.14/GitVersion_4.0.0-beta0014.zip

RUN unzip GitVersion_4.0.0-beta0014.zip  -d GitVersion && cd GitVersion

RUN echo \#!/bin/bash >/usr/bin/gitversion
RUN echo mono /GitVersion/GitVersion.exe \"\$\@\" >>/usr/bin/gitversion

RUN chmod +x /usr/bin/gitversion

RUN pip install --upgrade pip setuptools

COPY requirements.txt /tmp

RUN pip install -r /tmp/requirements.txt

RUN apt-get clean
